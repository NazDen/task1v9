package com.kpi.view;


import java.util.List;

public class View {


    public void printCountLoadCapacity(String companyName, int count) {

        System.out.println(String.format("Load capacity of all %s cargo airliners are %d kilos.\n", companyName, count));
    }

    public void printCountPassengersCapacity(String companyName, int count) {

        System.out.println(String.format("Capacity of all %s airliners are %d passengers.\n", companyName, count));
    }

    public void showSortedPlanes(String companyName, List planes) {
        System.out.println(String.format("All planes %s sorted by range:\n", companyName));
        planes.forEach(System.out::println);
        System.out.println();
    }

    public void printWrongInput() {
        System.out.println("Wrong input");
    }

    public void printBye() {
        System.out.println("Bye");
    }

    public void showAllSuitsPlanesByFuelUse(String companyName, List planes) {
        System.out.println(String.format("All planes %s which suits your input params:\n", companyName));
        planes.forEach(System.out::println);
        System.out.println();
    }

    public void printMenu() {
        System.out.println("1. Count load capacity of all planes;\n" +
                "2. Count passangers capacity of all planes;\n" +
                "3. Sort plane by range;\n" +
                "4. Find suitable plane by fuel usage;\n" +
                "Press 0 to exit app;\n" +
                "Please, choose your command:");
    }

    public void printHello(String name) {
        System.out.println(String.format("Hello, welcome to %s aircompany.", name));
    }

    public void printMinLimit() {
        System.out.println("Print minimal fuel usage for plane by hour:");
    }

    public void printMaxLimit() {
        System.out.println("Print maximal fuel usage for plane by hour:");
    }
}
