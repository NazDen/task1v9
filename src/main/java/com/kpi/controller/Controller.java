package com.kpi.controller;

import com.kpi.model.services.AirCompanyService;
import com.kpi.view.View;

import java.util.Scanner;

public class Controller {
    AirCompanyService airCompanyService;
    View view;

    public Controller(AirCompanyService airCompanyService, View view) {
        this.airCompanyService = airCompanyService;
        this.view = view;
    }

    public void run() {
        Scanner scanner = new Scanner(System.in);
        String s = "";
        view.printHello(airCompanyService.getCompanyName());
        do {
            view.printMenu();
            s = scanner.nextLine();
            switch (s) {
                case "0":
                    view.printBye();
                    break;
                case "1":
                    view.printCountLoadCapacity(airCompanyService.getCompanyName(), airCompanyService.countLoadCapacityOfAllPlanes());
                    break;
                case "2":
                    view.printCountPassengersCapacity(airCompanyService.getCompanyName(), airCompanyService.countPassengersCapacityOfAllPlanes());
                    break;
                case "3":
                    view.showSortedPlanes(airCompanyService.getCompanyName(), airCompanyService.sortAllPlanesByRange());
                    break;
                case "4":
                    view.printMinLimit();
                    int min = checkInput(scanner);
                    view.printMaxLimit();
                    int max = checkInput(scanner);
                    while (max < min){
                        view.printWrongInput();
                        max = checkInput(scanner);
                    }
                    view.showAllSuitsPlanesByFuelUse(airCompanyService.getCompanyName(), airCompanyService.getPlaneByFuelUse(min, max));
                    break;
                default:
                    view.printWrongInput();
            }
        }while (!s.equals("0"));
    }


    private int checkInput(Scanner sc) {
        String result = sc.nextLine();
        while (!result.matches("\\d{1,10}")) {
            view.printWrongInput();
            result = sc.nextLine();
        }
        return Integer.parseInt(result);
    }
}
