package com.kpi;

import com.kpi.controller.Controller;
import com.kpi.model.services.AirCompanyService;
import com.kpi.view.View;

public class App {
    public static void main(String[] args) {


        Controller controller = new Controller(new AirCompanyService(), new View());
        controller.run();

//        AirCompany airCompany = new AirCompany() {{
//            addPlane(new Airliner(PlaneSpecies.BOING_737_MAX_7));
//            addPlane(new Airliner(PlaneSpecies.BOING_747_400));
//            addPlane(new Airliner(PlaneSpecies.BOING_787_10));
//            addPlane(new Airliner(PlaneSpecies.AIRBUS_320));
//        }};

//
//        Float parameter "h" in meters must be greater than 0**
//      ** Float parameter "bounce" must be greater than 0 and less than 1**
//      ** Float parameter "window" must be less than h.**

    }
}

