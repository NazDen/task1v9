package com.kpi.model.services;

import com.kpi.data.PlaneTypes;
import com.kpi.model.dao.AirCompany;
import com.kpi.model.dao.AirCompanyDAO;
import com.kpi.model.entity.Aircraft;
import com.kpi.model.entity.Airliner;
import com.kpi.model.entity.CargoAirliner;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AirCompanyService {


    private AirCompanyDAO  iua = new AirCompany("IUA");

    public int countPassengersCapacityOfAllPlanes() {
        return iua.getAllPlanes().stream().filter(plane -> plane.getType().equals(PlaneTypes.AIRLINER))
                .map(plane -> (Airliner) plane).mapToInt(Airliner::getCountOfPassengers).sum();
    }


    public int countLoadCapacityOfAllPlanes() {
        return iua.getAllPlanes().stream().filter(plane -> plane.getType().equals(PlaneTypes.CARGO_AIRLINER))
                .map(plane -> (CargoAirliner) plane).mapToInt(CargoAirliner::getCurrentLoad).sum();
    }


    public List<Aircraft> sortAllPlanesByRange() {
        return iua.getAllPlanes().stream().sorted(Comparator.comparing(Aircraft::getRangeOfFlight))
                .collect(Collectors.toList());
    }


    public List<Aircraft> getPlaneByFuelUse(int min, int max) {
        return iua.getAllPlanes().stream().filter(aircraft -> aircraft.countUseOfFuel() > min && aircraft.countUseOfFuel() < max)
                .collect(Collectors.toList());
    }

    public String getCompanyName() {
        return iua.getName();
    }
}
