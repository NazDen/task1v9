package com.kpi.model.dao;

import com.kpi.data.PlaneSpecies;
import com.kpi.model.entity.Aircraft;
import com.kpi.model.entity.PlaneBuilder;

import java.util.ArrayList;
import java.util.List;

public class AirCompany implements AirCompanyDAO {
    private String name;
    private List<Aircraft> planes;

    public AirCompany(String name) {
        this.name = name;
        this.planes = new ArrayList<>();
        addPlanes();
    }

    private void addPlanes(){
        for (PlaneSpecies plane :
                PlaneSpecies.values()) {
            this.planes.add(PlaneBuilder.createAirliner(plane));
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Aircraft> getAllPlanes() {
        return planes;
    }

}
