package com.kpi.model.dao;

import com.kpi.model.entity.Aircraft;

import java.util.List;

public interface AirCompanyDAO {

    String getName();
    List<Aircraft> getAllPlanes();

}
