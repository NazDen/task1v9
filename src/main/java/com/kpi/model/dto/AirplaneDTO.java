package com.kpi.model.dto;

import com.kpi.data.PlaneSpecies;
import com.kpi.data.PlaneTypes;

import java.util.Optional;

public class AirplaneDTO {

    private final Optional<String> BRAND;
    private final Optional<String> MODEL;
    private final Optional<String> MODIFICATION;
    private final PlaneTypes TYPE;
    private final Optional<Integer> MAX_TANK_CAPACITY;
    private final Optional<Integer> MAX_RANGE;
    private final Optional<Integer> MAX_LOAD_CAPACITY;
    private final Optional<Integer> MAX_PASSENGER_CAPACITY;
    private final Optional<Integer> MAX_SPEED;

    public AirplaneDTO(PlaneSpecies planeSpecies) {
        this.BRAND = Optional.ofNullable(planeSpecies.getBRAND());
        this.MODEL = Optional.ofNullable(planeSpecies.getMODEL());
        this.MODIFICATION = Optional.ofNullable(planeSpecies.getMODIFICATION());
        this.TYPE = PlaneTypes.valueOf(Optional.of(planeSpecies.getTYPE()).orElse("UNKNOWN"));
        this.MAX_TANK_CAPACITY = Optional.ofNullable(planeSpecies.getMAX_TANK_CAPACITY());
        this.MAX_RANGE = Optional.ofNullable(planeSpecies.getMAX_RANGE());
        this.MAX_LOAD_CAPACITY = Optional.ofNullable(planeSpecies.getMAX_LOAD_CAPACITY());
        this.MAX_PASSENGER_CAPACITY = Optional.ofNullable(planeSpecies.getMAX_PASSENGER_CAPACITY());
        this.MAX_SPEED = Optional.ofNullable(planeSpecies.getMAX_SPEED());
    }

    public AirplaneDTO(String BRAND, String MODEL, String MODIFICATION, String type, Integer MAX_TANK_CAPACITY, Integer MAX_RANGE, Integer MAX_LOAD_CAPACITY, Integer MAX_PASSENGER_CAPACITY, Integer MAX_SPEED) {
        this.BRAND = Optional.ofNullable(BRAND);
        this.MODEL = Optional.ofNullable(MODEL);
        this.MODIFICATION = Optional.ofNullable(MODIFICATION);
        this.MAX_TANK_CAPACITY = Optional.ofNullable(MAX_TANK_CAPACITY);
        this.MAX_RANGE = Optional.ofNullable(MAX_RANGE);
        this.MAX_LOAD_CAPACITY = Optional.ofNullable(MAX_LOAD_CAPACITY);
        this.MAX_PASSENGER_CAPACITY = Optional.ofNullable(MAX_PASSENGER_CAPACITY);
        this.MAX_SPEED = Optional.ofNullable(MAX_SPEED);
        this.TYPE = PlaneTypes.valueOf(Optional.of(type).orElse("UNKNOWN"));
    }

    public String getBRAND() {
        return BRAND.orElse("No brand");
    }

    public String getMODEL() {
        return MODEL.orElse("No model");
    }

    public String getMODIFICATION() {
        return MODIFICATION.orElse("No modification");
    }

    public Integer getMAX_TANK_CAPACITY() {
        return MAX_TANK_CAPACITY.orElseThrow(()-> new IllegalArgumentException("No data of tank capacity"));
    }

    public Integer getMAX_RANGE() {
        return MAX_RANGE.orElseThrow(()-> new IllegalArgumentException("No data of max range"));
    }

    public Integer getMAX_LOAD_CAPACITY() {
        return MAX_LOAD_CAPACITY.orElseThrow(()-> new IllegalArgumentException("No data of load capacity"));
    }

    public Integer getMAX_PASSENGER_CAPACITY() {
        return MAX_PASSENGER_CAPACITY.orElse(0);
    }

    public Integer getMAX_SPEED() {
        return MAX_SPEED.orElseThrow(()-> new IllegalArgumentException("No data of max speed"));
    }

    public PlaneTypes getTYPE() {
        return TYPE;
    }
}
