package com.kpi.model.entity;

import com.kpi.data.PlaneSpecies;
import com.kpi.data.PlaneTypes;
import com.kpi.model.dto.AirplaneDTO;

public abstract class Aircraft {

    private String planeName;
    private int amountOfFuel;
    private int speed;
    private int rangeOfFlight;
    private PlaneTypes type;

    public Aircraft(AirplaneDTO plane) {
        this.planeName = String.format("%s %s %s", plane.getBRAND(), plane.getMODEL(), plane.getMODIFICATION());
        this.amountOfFuel = plane.getMAX_TANK_CAPACITY();// це просто вмістимість баку, не поточна кількість пального
        this.speed = plane.getMAX_SPEED();  // це максимальна швидкість, не поточна
        this.rangeOfFlight = plane.getMAX_RANGE();
        this.type = plane.getTYPE();
    }

    public String getPlaneName() {
        return planeName;
    }

    public void setPlaneName(String planeName) {
        this.planeName = planeName;
    }

    public int getAmountOfFuel() {
        return amountOfFuel;
    }

    public void setAmountOfFuel(int amountOfFuel) {
        this.amountOfFuel = amountOfFuel;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getRangeOfFlight() {
        return rangeOfFlight;
    }

    public void setRangeOfFlight(int rangeOfFlight) {
        this.rangeOfFlight = rangeOfFlight;
    }

    public int countUseOfFuel() {
        return amountOfFuel / rangeOfFlight * speed;
    }

    public PlaneTypes getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format(
                "planeName='%s', amountOfFuel=%d, speed=%d, rangeOfFlight=%d, type=%s",
                planeName, amountOfFuel, speed, rangeOfFlight, type);
    }
}
