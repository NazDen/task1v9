package com.kpi.model.entity;

import com.kpi.data.PlaneSpecies;
import com.kpi.data.PlaneTypes;
import com.kpi.model.dto.AirplaneDTO;

public class PlaneBuilder {// singleton

    public static Aircraft createAirliner(PlaneSpecies plane) {
        AirplaneDTO airplaneDTO = new AirplaneDTO(plane);
        if (airplaneDTO.getTYPE().equals(PlaneTypes.AIRLINER)) {
            return new Airliner(airplaneDTO);
        } else if (airplaneDTO.getTYPE().equals(PlaneTypes.CARGO_AIRLINER)) {
            return new CargoAirliner(airplaneDTO);
        } else {
            throw new IllegalArgumentException(String.format("Type is %s", PlaneTypes.UNKNOWN));
        }
    }
}
