package com.kpi.model.entity;

import com.kpi.data.PlaneSpecies;
import com.kpi.model.dto.AirplaneDTO;

public class CargoAirliner extends Aircraft {

    private int currentLoad;

    public CargoAirliner(AirplaneDTO plane) {
        super(plane);
        this.currentLoad = plane.getMAX_LOAD_CAPACITY();
    }

    public int getCurrentLoad() {
        return currentLoad;
    }

    public void setCurrentLoad(int currentLoad) {
        this.currentLoad = currentLoad;
    }
}
