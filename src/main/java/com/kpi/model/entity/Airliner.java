package com.kpi.model.entity;

import com.kpi.data.PlaneSpecies;
import com.kpi.model.dto.AirplaneDTO;

public class Airliner extends Aircraft {

    private int countOfPassengers;

    public Airliner(AirplaneDTO plane) {
        super(plane);
        this.countOfPassengers = plane.getMAX_PASSENGER_CAPACITY();
    }

    public int getCountOfPassengers() {
        return countOfPassengers;
    }

    public void setCountOfPassengers(int countOfPassengers) {
        this.countOfPassengers = countOfPassengers;
    }
}
