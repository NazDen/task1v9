package com.kpi.data;

public enum PlaneSpecies {

    BOING_747_400("boing", "747","400", "CARGO_AIRLINER", 220000, 14000, 400000, null, 920),
    BOING_737_MAX_7("boing", "737","max7", "AIRLINER", 25800, 7000, 80000, 172, 840),
    BOING_777_300("boing", "777","300", "CARGO_AIRLINER", 170000, 11000, 300000, null, 945),
    BOING_787_10("boing", "787","10", "AIRLINER", 127000, 12000, 250000, 440, 945),
    EMBRAER_195("embraer","195", null, "CARGO_AIRLINER", 16000, 2600, 49000, null, 890),
    EMBRAER_170("embraer","170", null, "AIRLINER", 12000, 3100, 36000, 78, 890),
    EMBRAER_145("embraer","145", null, "AIRLINER", 6500, 2800, 22000, 50, 830),
    AIRBUS_320("airbus", "320", null, "AIRLINER", 27000, 6100, 78000, 180, 890),
    AIRBUS_380("airbus", "380", null, "CARGO_AIRLINER", 310000, 15000, 560000, null, 1185),
    SAAB_2000("saab", "2000", null, "AIRLINER", 5900, 2100, 23000, 50, 685);

    private final String BRAND;
    private final String MODEL;
    private final String MODIFICATION;
    private final String TYPE;
    private final Integer MAX_TANK_CAPACITY;
    private final Integer MAX_RANGE;
    private final Integer MAX_LOAD_CAPACITY;
    private final Integer MAX_PASSENGER_CAPACITY;
    private final Integer MAX_SPEED;

    PlaneSpecies(String BRAND, String MODEL, String MODIFICATION, String TYPE, Integer MAX_TANK_CAPACITY, Integer MAX_RANGE, Integer MAX_LOAD_CAPACITY, Integer MAX_PASSENGER_CAPACITY, Integer MAX_SPEED) {
        this.BRAND = BRAND;
        this.MODEL = MODEL;
        this.MODIFICATION = MODIFICATION;
        this.TYPE = TYPE;
        this.MAX_TANK_CAPACITY = MAX_TANK_CAPACITY;
        this.MAX_RANGE = MAX_RANGE;
        this.MAX_LOAD_CAPACITY = MAX_LOAD_CAPACITY;
        this.MAX_PASSENGER_CAPACITY = MAX_PASSENGER_CAPACITY;
        this.MAX_SPEED = MAX_SPEED;
    }

    public String getBRAND() {
        return BRAND;
    }

    public String getMODEL() {
        return MODEL;
    }

    public String getMODIFICATION() {
        return MODIFICATION;
    }

    public Integer getMAX_TANK_CAPACITY() {
        return MAX_TANK_CAPACITY;
    }

    public Integer getMAX_RANGE() {
        return MAX_RANGE;
    }

    public Integer getMAX_LOAD_CAPACITY() {
        return MAX_LOAD_CAPACITY;
    }

    public Integer getMAX_PASSENGER_CAPACITY() {
        return MAX_PASSENGER_CAPACITY;
    }

    public Integer getMAX_SPEED() {
        return MAX_SPEED;
    }

    public String getTYPE() {
        return TYPE;
    }
}
